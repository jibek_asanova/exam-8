import React, {useEffect, useState} from 'react';
import axiosApi from "../../axiosApi";

const EditQuote = ({match, history}) => {
    const [editQuote, setEditQuote] = useState({
        author: '',
        category: '',
        text: ''
    });

    const onInputChange = e => {
        const {name, value} = e.target;

        setEditQuote(prev => ({
            ...prev,
            [name]: value
        }))
    };

    useEffect(() => {
        const fetchData = async () => {
            const response = await axiosApi.get('/quotes/' + match.params.id + '.json');
            setEditQuote(response.data);
            console.log('edit', response.data)
        };

        fetchData().catch(console.error);
    }, [match.params.id]);

    const changeQuote = (e) => {
        e.preventDefault();
        const fetchData = async () => {
            await axiosApi.put('/quotes/' + match.params.id + '.json', {
                author: editQuote.author,
                category: editQuote.category,
                text: editQuote.text,
            });
            history.replace('/');
        };
        fetchData().catch(console.error);
    }

    return (
        <form className="Form" onSubmit={changeQuote}>
            <label htmlFor="cars">Category:</label>

            <select name="category" onChange={onInputChange} className="Input"
            value={editQuote.category}>
                <option value="star-wars">Star Wars</option>
                <option value="famous-people">Famous people</option>
                <option value="saying">Saying</option>
                <option value="humor">Humor</option>
                <option value="motivation">Motivational</option>
            </select>
            <input
                className="Input"
                type="text"
                name="author"
                placeholder="Author"
                value={editQuote.author}
                onChange={onInputChange}
            />
            <textarea
                className="Input TextArea"
                name="text"
                placeholder="Description"
                value={editQuote.text}
                onChange={onInputChange}
            />
            <button type="submit" className="btn btn-primary">Save</button>
        </form>
    );
};

export default EditQuote;