import React, {useState} from 'react';
import axiosApi from "../../axiosApi";
import {category} from "../../Components/Category/Category";

import './CreateQuotes.css';

const CreateQuotes = ({history}) => {
    const [quote, setQuote] = useState({
        author: '',
        category: category[0].id,
        text: ''
    });

    const onInputChange = e => {
        const {name, value} = e.target;

        setQuote(prev => ({
            ...prev,
            [name]: value
        }))
    };

    const createQuote = async e => {
        e.preventDefault();

        try {
            await axiosApi.post('/quotes.json', quote);
            history.replace('/');
        } catch(e) {
            console.error();
        }
    }

    return (
        <form className="Form" onSubmit={createQuote}>
            <label htmlFor="cars">Category:</label>

            <select name="category" onChange={onInputChange} className="Input"
            >
                <option value="star-wars">Star Wars</option>
                <option value="famous-people">Famous people</option>
                <option value="saying">Saying</option>
                <option value="humor">Humor</option>
                <option value="motivation">Motivational</option>
            </select>
            <input
                className="Input"
                type="text"
                name="author"
                placeholder="Author"
                value={quote.author}
                onChange={onInputChange}
            />
            <textarea
                className="Input TextArea"
                name="text"
                placeholder="Description"
                value={quote.text}
                onChange={onInputChange}
            />
            <button type="submit" className="btn btn-primary">Save</button>
        </form>
    );
};

export default CreateQuotes;