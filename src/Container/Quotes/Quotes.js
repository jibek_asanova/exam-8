import React, {useEffect, useState} from 'react';
import axiosApi from "../../axiosApi";
import QuoteCard from "../../Components/QuoteCard/QuoteCard";
import './Quotes.css';

const Quotes = ({match, history}) => {
    const [allQuotes, setAllQuotes] = useState([]);


    const categoryId = match.params.id;

    const getResponse = (response) => {
        if(response !== null) {
            const response2 = Object.values(response);
            const keys = Object.keys(response);

            const quotesArray = [];

            for (let i = 0; i < keys.length; i++) {
                quotesArray.push({
                    id: keys[i],
                    author: response2[i].author,
                    category: response2[i].category,
                    text: response2[i].text
                })
            }
            setAllQuotes(quotesArray);
        }
    }
    useEffect(() => {
        const fetchData = async () => {
            if(categoryId) {
                const response = await axiosApi.get('/quotes.json?orderBy="category"&equalTo="' + categoryId + '"');

                getResponse(response.data);
            } else {
                const response = await axiosApi.get('/quotes.json');
                getResponse(response.data);
            }
        };
        fetchData().catch(console.error);
    }, [categoryId]);

    const editPost = (id) => {
        history.replace('/quotes/' + id + '/edit');
    };

    const deletePost = async (id) => {

        try{
            await axiosApi.delete('/quotes/' + id + '.json');
            const copyArray = [...allQuotes];
            function quoteId(index) {
                return index.id === id;
            }
            const index = allQuotes.findIndex(quoteId);
            copyArray.splice(index, 1);
            setAllQuotes(copyArray);
        }
        finally {
            history.replace('/');
        }
    };

    return allQuotes && (
        <div>
            {allQuotes.length ? allQuotes.map(quote => (
                <QuoteCard
                    key={quote.id}
                    quote={quote}
                    editPost={() => editPost(quote.id)}
                    deletePost={() => deletePost(quote.id)}
                />
            )) : <p>Please add some posts</p>}
        </div>
    );
};

export default Quotes;