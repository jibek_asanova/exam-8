import './App.css';
import Navbar from "./Components/Navbar/Navbar";
import Category from "./Components/Category/Category";
import {Route, Switch} from "react-router-dom";
import CreateQuotes from "./Container/CreateQuotes/CreateQuotes";
import Quotes from "./Container/Quotes/Quotes";
import EditQuote from "./Container/EditQuote/EditQuote";

function App() {
    return (
        <div className="App">
            <Navbar/>
            <Switch>
                <Route path="/add" component={CreateQuotes}/>
                <Route path="/quotes/:id/edit" component={EditQuote}/>
                <div className="MainPage">
                    <div className="Category">
                        <Category/>
                    </div>
                    <div className="Quotes">
                        <Route path="/" exact component={Quotes}/>
                        <Route path="/quotes/:id" component={Quotes}/>
                    </div>

                </div>
            </Switch>
        </div>
    );
}

export default App;
