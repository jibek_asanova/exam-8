import React from 'react';
import {NavLink} from "react-router-dom";

const Navbar = () => {
    return (
        <div className="Menu">
            <nav className="navbar navbar-light" style={{backgroundColor: '#e3f2fd'}}>
                <nav className="navbar navbar-expand-lg navbar-light ">
                    <div className="container-fluid">
                        <a className="navbar-brand" href="#">Quotes Central</a>
                        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                            <span className="navbar-toggler-icon"/>
                        </button>
                        <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
                            <div className="navbar-nav">
                                <NavLink exact to="/" className="nav-link" >Quotes</NavLink>
                                <NavLink to="/add" className="nav-link" >Submit a new quote</NavLink>
                            </div>
                        </div>
                    </div>
                </nav>
            </nav>
        </div>
    );
};

export default Navbar;