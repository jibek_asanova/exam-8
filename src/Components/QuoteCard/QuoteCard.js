import React from 'react';
import './QuoteCard.css';

const QuoteCard = ({quote, editPost, deletePost}) => {
    return (
        <div className="card"
        >
            <p>{quote.category}</p>
            <p>"{quote.text}"</p>
            <p>-{quote.author}</p>
            <div className="Buttons">
                <button type="button" className="btn btn-success" onClick={editPost}>Edit</button>
                <button type="button" className="btn btn-danger" onClick={deletePost}>Delete</button>
            </div>

        </div>
    );
};

export default QuoteCard;