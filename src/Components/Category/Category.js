import React from 'react';
import './Category.css';
import {NavLink} from "react-router-dom";

export const category = [
    {title: 'Star Wars', id: 'star-wars'},
    {title: 'Famous People', id: 'famous-people'},
    {title: 'Saying', id: 'saying'},
    {title: 'Humor', id: 'humor'},
    {title: 'Motivational', id: 'motivation'},
];

const Category = () => {

    return (
        <div className="Category">
            <ul className="CategoryList">
                <NavLink to="/">
                    <li>all</li>
                </NavLink>

                {category.map(item => (
                    <li key={item.id}>
                        <NavLink to={'/quotes/' + item.id}>
                            {item.title}
                        </NavLink>
                    </li>
                ))}
            </ul>
        </div>

    );
};

export default Category;