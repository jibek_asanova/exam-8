import axios from "axios";

const axiosApi = axios.create({
    baseURL: 'https://asanova-jibek-ex-8-default-rtdb.firebaseio.com/'
});

export default axiosApi;